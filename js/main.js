 AOS.init({
 	duration: 800,
 	easing: 'slide',
 	once: false
 });

jQuery(document).ready(function($) {

	"use strict";
	var singleShipmentUrl = 'http://cargoms.test/api/shipments/';

	$(".loader").delay(1000).fadeOut("slow");
  	$("#overlayer").delay(1000).fadeOut("slow");	

	var siteMenuClone = function() {

		$('.js-clone-nav').each(function() {
			var $this = $(this);
			$this.clone().attr('class', 'site-nav-wrap').appendTo('.site-mobile-menu-body');
		});


		setTimeout(function() {
			
			var counter = 0;
      $('.site-mobile-menu .has-children').each(function(){
        var $this = $(this);
        
        $this.prepend('<span class="arrow-collapse collapsed">');

        $this.find('.arrow-collapse').attr({
          'data-toggle' : 'collapse',
          'data-target' : '#collapseItem' + counter,
        });

        $this.find('> ul').attr({
          'class' : 'collapse',
          'id' : 'collapseItem' + counter,
        });

        counter++;

      });

    }, 1000);

		$('body').on('click', '.arrow-collapse', function(e) {
      var $this = $(this);
      if ( $this.closest('li').find('.collapse').hasClass('show') ) {
        $this.removeClass('active');
      } else {
        $this.addClass('active');
      }
      e.preventDefault();  
      
    });

		$(window).resize(function() {
			var $this = $(this),
				w = $this.width();

			if ( w > 768 ) {
				if ( $('body').hasClass('offcanvas-menu') ) {
					$('body').removeClass('offcanvas-menu');
				}
			}
		})

		$('body').on('click', '.js-menu-toggle', function(e) {
			var $this = $(this);
			e.preventDefault();

			if ( $('body').hasClass('offcanvas-menu') ) {
				$('body').removeClass('offcanvas-menu');
				$this.removeClass('active');
			} else {
				$('body').addClass('offcanvas-menu');
				$this.addClass('active');
			}
		}) 

		// click outisde offcanvas
		$(document).mouseup(function(e) {
	    var container = $(".site-mobile-menu");
	    if (!container.is(e.target) && container.has(e.target).length === 0) {
	      if ( $('body').hasClass('offcanvas-menu') ) {
					$('body').removeClass('offcanvas-menu');
				}
	    }
		});
	}; 
	siteMenuClone();


	var sitePlusMinus = function() {
		$('.js-btn-minus').on('click', function(e){
			e.preventDefault();
			if ( $(this).closest('.input-group').find('.form-control').val() != 0  ) {
				$(this).closest('.input-group').find('.form-control').val(parseInt($(this).closest('.input-group').find('.form-control').val()) - 1);
			} else {
				$(this).closest('.input-group').find('.form-control').val(parseInt(0));
			}
		});
		$('.js-btn-plus').on('click', function(e){
			e.preventDefault();
			$(this).closest('.input-group').find('.form-control').val(parseInt($(this).closest('.input-group').find('.form-control').val()) + 1);
		});
	};
	// sitePlusMinus();


	var siteSliderRange = function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 500,
      values: [ 75, 300 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
      " - $" + $( "#slider-range" ).slider( "values", 1 ) );
	};
	// siteSliderRange();


	

	var siteCarousel = function () {
		if ( $('.nonloop-block-13').length > 0 ) {
			$('.nonloop-block-13').owlCarousel({
		    center: false,
		    items: 1,
		    loop: true,
				stagePadding: 0,
		    margin: 0,
		    smartSpeed: 1000,
		    autoplay: true,
		    nav: true,
				navText: ['<span class="icon-arrow_back">', '<span class="icon-arrow_forward">'],
		    responsive:{
	        600:{
	        	margin: 0,
	        	nav: true,
	          items: 2
	        },
	        1000:{
	        	margin: 0,
	        	stagePadding: 0,
	        	nav: true,
	          items: 2
	        },
	        1200:{
	        	margin: 0,
	        	stagePadding: 0,
	        	nav: true,
	          items: 3
	        }
		    }
			});
		}

		$('.slide-one-item').owlCarousel({
	    center: false,
	    items: 1,
	    loop: true,
			stagePadding: 0,
	    margin: 0,
	    smartSpeed: 1500,
	    autoplay: true,
	    pauseOnHover: false,
	    dots: true,
	    nav: true,
	    navText: ['<span class="icon-keyboard_arrow_left">', '<span class="icon-keyboard_arrow_right">']
	  });

	  if ( $('.owl-all').length > 0 ) {
			$('.owl-all').owlCarousel({
		    center: false,
		    items: 1,
		    loop: false,
				stagePadding: 0,
		    margin: 0,
		    autoplay: false,
		    nav: false,
		    dots: true,
		    touchDrag: true,
  			mouseDrag: true,
  			smartSpeed: 1000,
				navText: ['<span class="icon-arrow_back">', '<span class="icon-arrow_forward">'],
		    responsive:{
	        768:{
	        	margin: 30,
	        	nav: false,
	        	responsiveRefreshRate: 10,
	          items: 1
	        },
	        992:{
	        	margin: 30,
	        	stagePadding: 0,
	        	nav: false,
	        	responsiveRefreshRate: 10,
	        	touchDrag: false,
  					mouseDrag: false,
	          items: 3
	        },
	        1200:{
	        	margin: 30,
	        	stagePadding: 0,
	        	nav: false,
	        	responsiveRefreshRate: 10,
	        	touchDrag: false,
  					mouseDrag: false,
	          items: 3
	        }
		    }
			});
		}
		
	};
	siteCarousel();

	

	var siteCountDown = function() {

		$('#date-countdown').countdown('2020/10/10', function(event) {
		  var $this = $(this).html(event.strftime(''
		    + '<span class="countdown-block"><span class="label">%w</span> weeks </span>'
		    + '<span class="countdown-block"><span class="label">%d</span> days </span>'
		    + '<span class="countdown-block"><span class="label">%H</span> hr </span>'
		    + '<span class="countdown-block"><span class="label">%M</span> min </span>'
		    + '<span class="countdown-block"><span class="label">%S</span> sec</span>'));
		});
				
	};
	// siteCountDown();

	var siteDatePicker = function() {

		if ( $('.datepicker').length > 0 ) {
			$('.datepicker').datepicker();
		}

	};
	// siteDatePicker();

	var siteSticky = function() {
		$(".js-sticky-header").sticky({topSpacing:0});
	};
	siteSticky();

	// navigation
  var OnePageNavigation = function() {
    var navToggler = $('.site-menu-toggle');

   	$("body").on("click", ".main-menu li a[href^='#'], .smoothscroll[href^='#'], .site-mobile-menu .site-nav-wrap li a[href^='#']", function(e) {
      e.preventDefault();

      var hash = this.hash;

      $('html, body').animate({
        'scrollTop': $(hash).offset().top - 50
      }, 600, 'easeInOutExpo', function() {
        // window.location.hash = hash;

      });

    });
  };
  OnePageNavigation();

  var siteScroll = function() {

  	

  	$(window).scroll(function() {

  		var st = $(this).scrollTop();

  		if (st > 100) {
  			$('.js-sticky-header').addClass('shrink');
  		} else {
  			$('.js-sticky-header').removeClass('shrink');
  		}

  	}) 

  };
  siteScroll();


  var counter = function() {
		
		$('#about-section').waypoint( function( direction ) {

			if( direction === 'down' && !$(this.element).hasClass('ftco-animated') ) {

				var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',')
				$('.number > span').each(function(){
					var $this = $(this),
						num = $this.data('number');
					$this.animateNumber(
					  {
					    number: num,
					    numberStep: comma_separator_number_step
					  }, 7000
					);
				});
				
			}

		} , { offset: '95%' } );

	}
	counter();

	var searchShipment = function() {
		$('#shipment_search').on('submit', function(event) {
			event.preventDefault();
			var request;

			var tracking_id = $('#tracking_id').val();
			if (!tracking_id) {
				alert2('info', 'Info', 'Please provide tracking number');
				return;
			}
			
			if(request) {
				request.abort();
			}

			request = $.ajax({
				url: singleShipmentUrl + tracking_id,
				type: 'GET',
				crossDomain: true,
				headers: { 'Access-Control-Allow-Origin': '*' },
			});

			request.done(function(resp, status, xhr) {
				//Save tracking id to localStorage
				if(status === 'success') {
					localStorage.setItem('tracking_id', tracking_id);
					window.location.replace('./shipment-details.html');
				} else {
					alert2('error', 'OOPS!', resp.error);
				}
			});

			request.fail(function(_, __, errorMessage) {
				if (_.status === 404) {
					alert2('error', 'OOPS!', _.responseJSON.error);
				} else {
					alert2('error', 'OOPS!', 'An unexpected error occurred. Please try again later.');
				}
			});

		});
	};
	searchShipment();

	var fetchShipment = function() {
		setTimeout(setShipment, 100);

		function setShipment() {
			var request;
			var tracking_id = localStorage.getItem('tracking_id');
			if (!tracking_id) {
				return;
			}

			if(request) {
				request.abort();
			}

			request = $.ajax({
				url: singleShipmentUrl + tracking_id,
				type: 'GET',
				crossDomain: true,
				headers: { 'Access-Control-Allow-Origin': '*' },
			});

			request.done(function(resp, status, xhr) {
				// console.log(resp);
				// console.log('City ', resp.shipment.shipment_info.origin.city);
				$('#tracking_id').text(resp.shipment.tracking_id ?? '');
				$('#update_time').text(resp.shipment.histories[resp.shipment.histories.length - 1].date + ' ' + resp.shipment.histories[resp.shipment.histories.length - 1].time);
				$('#status').text(resp.shipment.histories[resp.shipment.histories.length - 1].shipment_statuses[0].name);
				$('#exp_dev_date').text(resp.shipment.shipment_info.expected_delivery_date);
				$('#destination').text(resp.shipment.shipment_info.destination.city + ', ' + resp.shipment.shipment_info.destination.country.name);
				$('#destination-flag').append(`<img src="${resp.shipment.shipment_info.destination.country.flag_url}" width="70">`);
				$('#origin').text(resp.shipment.shipment_info.origin.city + ', ' + resp.shipment.shipment_info.origin.country.name);
				$('#origin-flag').append(`<img src="${resp.shipment.shipment_info.origin.country.flag_url}" width="70">`);
				$('#weight').text(resp.shipment.packages[0].weight + ' ' + resp.units.weight);
				$('#piece-type').text(resp.shipment.packages[0].piece_type.name);
				$('#qty').text(resp.shipment.packages[0].quantity);
				$('#description').text(resp.shipment.packages[0].description);
				$('#agent').text(resp.shipment.agent.name);
				$('#agent-number').text(resp.shipment.agent.phone);
				$('#client').text(resp.shipment.client.name);
				$('#client-number').text(resp.shipment.client.phone);
				$('#receiver-info').append(receiverTemplate(resp.shipment.receiver));
				$('#shipper-info').append(shipperTemplate(resp.shipment.shipper));
				// $('#piece-type').text(resp.shipment.packages[0].quantity + ' ' + resp.shipment.packages[0].piece_type.name);
				if (resp.shipment.shipment_info.cost === '0') {
					$('#cost').text('N/A');
				} else {
					$('#cost').text(resp.shipment.shipment_info.cost);
				}
				// 

				var template = `<div class="tracking-item">
									<div class="tracking-icon status-intransit">
									<svg class="svg-inline--fa fa-circle fa-w-16" aria-hidden="true" data-prefix="fas"
										data-icon="circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
										data-fa-i2svg="">
										<path fill="currentColor"
										d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z"></path>
									</svg>
									<!-- <i class="fas fa-circle"></i> -->
									</div>
									<div class="tracking-date">${resp.shipment.histories[0].date}<span>${resp.shipment.shipment_info.departure_time}</span></div>
									<div class="tracking-content">${resp.shipment.histories[0].remark}<span class="text-uppercase font-size-18"><img width="32" src="${resp.shipment.shipment_info.origin.country.flag_url}"> ${resp.shipment.shipment_info.origin.city}, ${resp.shipment.shipment_info.origin.country.name}</span></div>
								</div>`;
				$('.tracking-list').append(template);

				if(resp.shipment.histories.length > 1) {
					for(var i = 1; i < resp.shipment.histories.length; i++) {
						var date = resp.shipment.histories[i].date;
						var time = resp.shipment.histories[i].time;
						var description = resp.shipment.histories[i].remark;
						var city = resp.shipment.histories[i].locations[0].city;
						var country = resp.shipment.histories[i].locations[0].country;

						$('.tracking-list').prepend(renderTemplate(date, time, description, city, country));
					}
				}
			});

			request.fail(function(_, __, errorMessage) {
				alert2('error', 'Error', 'An unexpected error occurred. Please try again later.');
			});
		}
	}
	fetchShipment();

	function renderTemplate(date, time, description, city, country) {
		return `<div class="tracking-item">
					<div class="tracking-icon status-intransit">
					<svg class="svg-inline--fa fa-circle fa-w-16" aria-hidden="true" data-prefix="fas"
						data-icon="circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
						data-fa-i2svg="">
						<path fill="currentColor"
						d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z"></path>
					</svg>
					<!-- <i class="fas fa-circle"></i> -->
					</div>
					<div class="tracking-date">${date}<span>${time}</span></div>
					<div class="tracking-content">${description}<span class="text-uppercase font-size-18"><img width="32" src="${country.flag_url}"> ${city}, ${country.name}</span></div>
				</div>`;
	}

	function receiverTemplate(receiver)
	{
		return `<span><b>Name: </b>${receiver.name || 'N/A'}</span><br/>
				<span><b>Address:</b> ${receiver.address || 'N/A'}</span><br/>
				<span><b>Mobile:</b> ${receiver.phone || 'N/A'}</span><br/>
				<span><b>Email:</b> ${receiver.email || 'N/A'}</span>`;
	}

	function shipperTemplate(shipper) {
		return `<span><b>Name: </b>${shipper.name || 'N/A'}</span><br/>
				<span><b>Address:</b> ${shipper.address || 'N/A'}</span><br/>
				<span><b>Mobile:</b> ${shipper.phone || 'N/A'}</span><br/>
				<span><b>Email:</b> ${shipper.email || 'N/A'}</span>`;
	}

	function toastAlert(type = "info", message = "message") {
		type = type.toLowerCase();

		var option = {
			closeButton: true,
			debug: false,
			newestOnTop: true,
			progressBar: true,
			positionClass: "toast-top-right",
			preventDuplicates: false,
			onclick: null,
			showDuration: "300",
			hideDuration: "1000",
			timeOut: "3000",
			extendedTimeOut: "1000",
			showEasing: "swing",
			hideEasing: "linear",
			showMethod: "fadeIn",
			hideMethod: "fadeOut",
		};

		// var canShow = false;
		switch (type) {
			case "success":
				toastr.success(message, "Success", option);
				break;
			case "info":
				toastr.info(message, "Info", option);
				break;
			case "error":
				toastr.error(message, "Error", option);
				break;
			case "warning":
				toastr.warning(message, "Warning", option);
				break;
		}
	};

	function alert2(type='info', title='info', message='') {
		var type = type.toLowerCase();
		var alertTypes = ['info', 'warning', 'success', 'error', 'question'];
		var check = alertTypes.find(_type => type === _type);

		if (!check) {
			type = 'info';
		}

		 Swal.fire({
			 icon: type,
			 title: title,
			 text: message,
			 confirmButtonText: 'Dismiss'
		 });
	}

	var handleNewsLetterSub = function() {
		$('#button-addon2').on('click', function(event) {
			event.preventDefault();
			var email = $('#email').val();
			if (email) {
				toastAlert('success', 'You have been successfully opted in for our news letter');
				email.val('');
			} else {
				toastAlert('error', 'Please provide a valid email address and try again.');
			}
		})
	}

	handleNewsLetterSub();

});
